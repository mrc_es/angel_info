
# Esta es la imagen y el registro de donde le haces pull.
docker pull splunk/splunk:latest

#  Use splunk_nifi_network aquella vez para conectar 
#+ el contenedor de apache nifi y splunk a través de esa red
#+ por lo que el parámetro --net=splunk_nifi_network no es
#+ necesario.
#  Se mapea a alguna carpeta sólo para posteriormente crear 
#+ ahi archivos que van a usarse para generar logs controlados.
docker run -d \
	--net=splunk_nifi_network \
	-p 8000:8000 \
	-e 'SPLUNK_START_ARGS=--accept-license' \
	-e 'SPLUNK_PASSWORD=<tu contrasena>' \
	-v "<ruta a mapear>":/mnt/docker_splunk \
	--name splunk_personal \
	splunk/splunk:latest

#  Esta consulta sencilla solo monitorea los archivos en la carpeta /mnt/splunk,
#+ que mapeamos previamente en la definición del contenedor, en la interfaz en 
#+ el menu de monitoreo.
search source="/mnt/docker_splunk/*" index="splunk_nifi_index" | head 100  #  La clave "index" es el índice al que
                                                                           #+ hace referencia, por lo general el indice
                                                                           #+ tiene el nombre de "main". Sólo que, cuando yo hice
                                                                           #+ mis pruebas, cree uno nuevo con el nombre de 
                                                                           #+ "splunk_nifi_index".
#  Por lo general, la consulta por defecto ya viene con todos los parámetros necesarios, 
#+ en ese caso basta sólo con cambiar el parámetro "source" al deseado y el "index", a
#+ menos que se tenga por defecto el de "main".

#  Esto es para detener y borrar el contenedor
#+ por si quieres hacer otro.
printf 'stop\nrm' | xargs -I {} bash -c 'docker container {} splunk_personal'

#Un script para ver una bonita gráfica de logs es este.
#[START] script
#!/bin/bash

: << __docstring__

		Lo único que hace este script es crear archivos y abrirlos para escritura.
	La creación de estos archivos y su escritura describe el comportamiento de una
	curva que puedes ver en el reporte de registros de splunk.

		Un problema con splunk es que, si borras los archivos para volverlos a crear, cuando 
	vuelves a tratar de monitorear la creación y escritura de archivos, ya no verás los logs. Tal vez
	porque ya se registraron y tendrías que volver a crear un nuevo indice, no lo sé. Sin embargo, 
	para poder ver un nuevo reporte puedes simplemente añadir una extensión a la variable "ruta", 
	quizas un "archivo.1", "archivo.2", etc.

__docstring__

export ruta=<ruta>/archivo
export pow=2

for i in {1..100}
do
    
    sleep 0.5
    plus_pow2=$( bc <<< "$i^$pow" )
    echo $plus_pow2

    seq 1 $plus_pow2 | xargs -n1 | xargs -I {} bash -c "echo pasada ${i}.{} >> ${ruta}.${i}"

done
#[END] script

# Para que veas las lineas que tienen los archivos que creaste
find <ruta> -mindepth 1 -not -name "<nombre que quieras excluir>" | xargs -I {} wc -l {}
